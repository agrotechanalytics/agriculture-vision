[2020-07-29 19:28:56,614 INFO] Loaded configuration file config/agri-resnet101dilated-ibn@a-deeplab-low_feat@3-bce+dice+lovasz-aug-warmup@2000.yaml
[2020-07-29 19:28:56,615 INFO] Running with config:
DATASET:
  aspect_limit: (-1.0, 1.0)
  hue_shift_limit: (0, 0)
  imgMaxSize: 1000
  imgSizes: (300, 375, 450, 525, 600)
  img_downsampling_rate: 1.0
  list_test: 
  list_train: ['./data/agri-trn.odgt']
  list_val: ./data/agri-val.odgt
  num_class: 7
  padding_constant: 8
  random_flip: True
  root_dataset: data/
  sat_shift_limit: (0, 0)
  scale_limit: (-1.0, 1.0)
  segm_downsampling_rate: 1.0
  shift_limit: (-1.0, 1.0)
  train_channels: rgbn
  val_channels: ['rgbn']
  val_shift_limit: (0, 0)
DIR: ckpt/agri-resnet101dilated-ibn@a-deeplab-low_feat@3-bce+dice+lovasz-aug-warmup@2000-2020-07-29-Wed-19:28:56:612820
LOSS:
  bce: 1.0
  dice: 1.0
  focal: 0.0
  iou: 0.0
  lovasz: 1.0
MODEL:
  arch: deeplab
  backbone: resnet101
  fc_dim: 2048
  fp16: True
  ibn_mode: a
  interpolate_before_lastconv: False
  num_low_level_feat: 3
  os: 16
  pred_downsampling_rate: 1.0
TEST:
  batch_size_per_gpu: 1
  checkpoint: epoch_20.pth
  result: ./
TRAIN:
  batch_size_per_gpu: 8
  beta1: 0.9
  deep_sup_scale: 0.4
  disp_iter: 20
  fix_bn: False
  iter_decay: 20000
  iter_static: 8000
  iter_warmup: 2000
  lr: 0.01
  lr_pow: 0.9
  mixup_alpha: 0.0
  optim: SGD
  resume_checkpoint: 
  seed: 304
  shuffle_R_and_N: 0.0
  start_epoch: 0
  weight_decay: 0.0005
  workers: 12
VAL:
  batch_size_per_gpu: 8
  checkpoint: epoch_20.pth
  visualize: False
  visualized_label: 
  visualized_pred: 
[2020-07-29 19:29:00,276 INFO] World Size: 1
[2020-07-29 19:29:00,276 INFO] TRAIN.epoch_iters: 1612
[2020-07-29 19:29:00,276 INFO] TRAIN.sum_bs: 8
[2020-07-29 19:29:00,276 INFO] VAL.epoch_iters: 553
[2020-07-29 19:29:00,276 INFO] VAL.sum_bs: 8
[2020-07-29 19:29:00,276 INFO] TRAIN.num_epoch: 18
[2020-07-29 19:29:28,819 INFO] TRAIN >> Epoch: [1][0/1612], Time: 28.54, Data: 5.99, lr: 0.000005, Loss: 1.613199, bce_Loss: 0.506039, dice_Loss: 0.821163, lovasz_Loss: 3.512395
